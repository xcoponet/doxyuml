// Cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "inc/rectangle.h"

int main()
{
    std::cout << "Hello World!\n"; 

    Geometry::Rectangle rec( 10, 2 );

    std::cout << rec.area() << std::endl;

    return 0;
}

