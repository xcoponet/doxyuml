from graphviz import Digraph

dot = Digraph(name="Couriers")
dot.attr("node", shape="record")
dot.attr("edge",arrowtail="empty")

dot.node('Courier',label="{Courier|+ name : string\l+ home_country : "
         "string\l|+ calculateShipping() : float\l+ ship(): boolean\l}")
dot.node('Monotype',"{MonotypeDelivery|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")
dot.node('Pigeon',"{PigeonPost|\l|+ ship(): boolean\l}")

dot.g


dot.edge("Courier", "Pigeon", dir="back")
dot.edge("Courier", "Monotype", dir="back")

print(dot.source)

dot.render('test.gv', view=True)